demo = {


    initDashboardPageCharts: function() {

        if ($('#dailySalesChart').length != 0 || $('#dailySuhuChart').length != 0 || $('#completedTasksChart').length != 0) {
            /* ----------==========     Daily Sales Chart initialization    ==========---------- */

            dataDailySalesChart = {
                labels: ['M', 'T', 'W', 'T', 'F', 'S', 'S'],
                series: [
                    [12, 14, 7, 17, 23, 18, 38]
                ]
            };

            optionsDailySalesChart = {
                lineSmooth: Chartist.Interpolation.cardinal({
                    tension: 0
                }),
                low: 0,
                high: 50, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
                chartPadding: {
                    top: 0,
                    right: 0,
                    bottom: 0,
                    left: 0
                },
            }

            var dailySalesChart = new Chartist.Line('#dailySalesChart', dataDailySalesChart, optionsDailySalesChart);

            md.startAnimationForLineChart(dailySalesChart);


            /* ----------==========     Hourly Suhu Chart initialization    ==========---------- */

            dataDailySuhuChart = {
                labels: ['M', 'T', 'W', 'T', 'F', 'S', 'S'],
                series: [
                    [12, 14, 7, 17, 23, 18, 38]
                ]
            };

            optionsDailySuhuChart = {
                lineSmooth: Chartist.Interpolation.cardinal({
                    tension: 0
                }),
                low: 0,
                high: 50, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
                chartPadding: {
                    top: 0,
                    right: 0,
                    bottom: 0,
                    left: 0
                },
            }

            var dailySuhuChart = new Chartist.Line('#dailySuhuChart', dataDailySuhuChart, optionsDailySuhuChart);

            md.startAnimationForLineChart(dailySuhuChart);



            /* ----------==========     Completed Tasks Chart initialization    ==========---------- */

            dataCompletedTasksChart = {
                labels: ['12p', '3p', '6p', '9p', '12p', '3a', '6a', '9a'],
                series: [
                    [230, 750, 450, 300, 280, 240, 200, 190]
                ]
            };

            optionsCompletedTasksChart = {
                lineSmooth: Chartist.Interpolation.cardinal({
                    tension: 0
                }),
                low: 0,
                high: 1000, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
                chartPadding: {
                    top: 0,
                    right: 0,
                    bottom: 0,
                    left: 0
                }
            }

            var completedTasksChart = new Chartist.Line('#completedTasksChart', dataCompletedTasksChart, optionsCompletedTasksChart);

            // start animation for the Completed Tasks Chart - Line Chart
            md.startAnimationForLineChart(completedTasksChart);

        }
    },


}

var ctx = document.getElementById('moisture');
var moisture = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ['07', '08', '09', '10', '11', '12'],
        datasets: [{
            data: [100, 200, 100, 150, 250, 300],
            fill: false,
            backgroundColor: "green",
            borderColor: "green",
            borderWidth: 3
        }]
    },
    options: {
        legend: {
            display: false
        },

        elements: {
            line: {
                tension: 0
            }
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});


var ctx = document.getElementById('suhu');
var suhu = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ['07', '08', '09', '10', '11', '12'],
        datasets: [{
            label: "Temprature",
            data: [35, 40, 37, 45, 47, 39],
            fill: false,
            backgroundColor: 'red',
            borderColor: 'red',
            borderWidth: 3
        }, {
            label: "humidity",
            data: [70, 30, 27, 50, 60, 70],
            fill: false,
            backgroundColor: 'blue',
            borderColor: 'blue',
            borderWidth: 3
        }]
    },
    options: {


        elements: {
            line: {
                tension: 0
            }
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

var ctx = document.getElementById('flow');
var flow = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ['07', '08', '09', '10', '11', '12'],
        datasets: [{
            data: [3, 5, 2, 3, 5, 7],
            fill: false,
            backgroundColor: "orange",
            borderColor: "orange",
            borderWidth: 3
        }]
    },
    options: {
        legend: {
            display: false
        },

        elements: {
            line: {
                tension: 0
            }
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});