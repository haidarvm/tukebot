
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <!-- <div class="col-lg-4 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-warning card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">fireplace</i>
                  </div>
                  <p class="card-category">Temprature</p>
                  <h3 class="card-title">Temprature
                    <small>28 &#8451;</small>
                  </h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">date_range</i> Last 24 Hours
                  </div>
                </div>
              </div>
            </div> -->
            <div class="col-lg-4 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">eco</i>
                  </div>
                  <p class="card-category">Status</p>
                  <h3 class="card-title"> Status Device 1 <a href="<?=site_url();?>dashboard/on/1" class="btn btn-sm btn-warning">On</a></h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">date_range</i> Last 24 Hours
                  </div>
                </div>
              </div>
            </div>
            <!-- <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-danger card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">invert_colors</i>
                  </div>
                  <p class="card-category">Flow</p>
                  <h3 class="card-title">Flow <small>80</small></h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">date_range</i> Last 24 Hours
                  </div>
                </div>
              </div>
            </div> -->
            <div class="col-lg-4 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">eco</i>
                  </div>
                  <p class="card-category">Status</p>
                  <h3 class="card-title"> Status Device 2 <a href="<?=site_url();?>dashboard/on/2" class="btn btn-sm btn-warning">On</a></h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">date_range</i> Last 24 Hours
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="card card-chart">
                <div class="card-header card-header">
                  <canvas id="device01" width="100" height="50"></canvas>
                </div>
                <div class="card-body">
                  <h4 class="card-title">Moisture Device 1</h4>
                  <p class="card-category">
                    <span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in today moisture.</p>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">access_time</i> updated 4 minutes ago
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="card card-chart">
                <div class="card-header card-header">
                  <canvas id="device02" width="100" height="50"></canvas>
                </div>
                <div class="card-body">
                  <h4 class="card-title">Moisture Device 2</h4>
                  <p class="card-category">
                    <span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in today moisture.</p>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">access_time</i> updated 4 minutes ago
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12 col-md-12">
              <div class="card">
                <div class="card-header card-header-warning">
                  <h4 class="card-title">Device</h4>
                  <p class="card-category">New employees on 15th September, 2016</p>
                </div>
                <div class="card-body table-responsive">
                  <table class="table table-hover">
                    <thead class="text-warning">
                      <th>Device</th>
                      <th>Moisture</th>
                      <th>Humidity</th>
                      <th>Temprature</th>
                      <th>Date</th>
                    </thead>
                    <tbody>
                    <?php foreach($devices as $device) { ?>
                      <tr>
                        <td><?=$device->id_controller;?></td>
                        <td><?=$device->moisture_contain;?></td>
                        <td><?=$device->humidity;?></td>
                        <td><?=$device->temperature;?></td>
                        <td><?=$device->date;?></td>
                      </tr>
                    <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

<script src="<?=base_url();?>assets/js/plugins/Chart.min.js"></script>
<script>
// device 01
var ctx = document.getElementById('device01');
var device01 = new Chart(ctx, {
    type: 'line',
    data: {
        labels: [<?php foreach($device01 as $one) { ?>
              <?php echo "'". $one->second ."',"; } ?>],
        datasets: [{
            data: [<?php foreach($device01 as $one) { ?>
              <?php echo "'". $one->moisture_contain ."',"; } ?>],
            fill: false,
            backgroundColor: "green",
            borderColor: "green",
            borderWidth: 3
        }]
    },
    options: {
        legend: {
            display: false
        },

        elements: {
            line: {
                tension: 0
            }
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

// device 02
var ctx = document.getElementById('device02');
var device02 = new Chart(ctx, {
    type: 'line',
    data: {
        labels: [
          <?php foreach($device01 as $one) { ?>
              <?php echo "'". $one->second ."',"; } ?>
        ],
        datasets: [{
            data: [ <?php foreach($device01 as $one) { ?>
              <?php echo "'". $one->moisture_contain ."',"; } ?>],
            fill: false,
            backgroundColor: "blue",
            borderColor: "blue",
            borderWidth: 3
        }]
    },
    options: {
        legend: {
            display: false
        },

        elements: {
            line: {
                tension: 0
            }
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
setTimeout(function(){
   window.location.reload(1);
}, 10000);
</script>