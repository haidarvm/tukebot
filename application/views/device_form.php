<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title">Edit Device</h4>
                        <p class="card-category">Complete your profile</p>
                    </div>
                    <div class="card-body">
                        <form action="<?php echo site_url().'device/'. $action;?>" method="POST">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" name="nama"
                                            value="<?php echo !empty($device) ? $device->nama : "";?>"
                                            class="form-control">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Mode</label>
                                        <select class="form-control" name="mode" data-style="btn btn-link" id="exampleFormControlSelect1">
                                            <option <?php echo !empty($device->mode) == 0 ? "selected" : "";?> value="0">0</option>
                                            <option <?php echo !empty($device->mode) == 1 ? "selected" : "";?> value="1">1</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Link</label>
                                        <input type="text" name="link"
                                            value="<?php echo !empty($device) ? $device->link : "";?>"
                                            class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-check form-check-radio">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" id="exampleRadios1" name="active"
                                        value="0" <?php echo !empty($device->active) == 1 ? "checked" : "";?>>
                                    Device is active
                                    <span class="circle">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>
                            <div class="form-check form-check-radio">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" id="exampleRadios2" name="active"
                                        value="1" <?php echo !empty($device->status)  == 0  ? "checked" : "";?>>
                                    Device is non active
                                    <span class="circle">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>
                            <div class="form-check form-check-radio">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" id="exampleRadios1" name="status"
                                        value="0">
                                    Device is On
                                    <span class="circle">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>
                            <div class="form-check form-check-radio">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" id="exampleRadios2" name="status"
                                        value="1" checked>
                                    Device is OFF
                                    <span class="circle">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>
                            <button type="submit" class="btn btn-primary pull-right">Update Config</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card card-profile">
                    <div class="card-header card-header-danger card-header-icon">
                        <div class="card-icon">
                            <i class="material-icons">settings</i>
                        </div>
                    </div>
                    <div class="card-body">
                        <h6 class="card-category text-gray">Device</h6>
                        <h4 class="card-title"><?php echo !empty($device) ? $device->nama : "";?></h4>
                        <p class="card-description">
                            Don't be scared of the truth because we need to restart the human foundation in truth And I
                            love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
                        </p>
                        <a href="#" class="btn btn-primary btn-round">Edit</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>