<div class="content">
    <div class="container-fluid">
    <div class="row">
            <div class="col-lg-12 col-md-12">
              <div class="card">
                <div class="card-header card-header-info">
                  <h4 class="card-title">Device</h4>
                  <p class="card-category">New employees on 15th September, 2016</p>
                    <a href="<?=site_url();?>device/add" class="btn btn-danger">Add</a>
                </div>
                <div class="card-body table-responsive">
                  <table class="table table-hover">
                    <thead class="text-warning">
                      <th>ID</th>
                      <th>Name</th>
                      <th>Link</th>
                      <th>Active</th>
                      <th>Status</th>
                      <th>Action</th>
                    </thead>
                    <tbody>
                    <?php foreach($devices as $device) {?>
                      <tr>
                        <td><?=$device->id;?></td>
                        <td><?=$device->nama;?></td>
                        <td><?=$device->link;?></td>
                        <td><?=$device->active == 1 ? 'active' : 'non-active';?></td>
                        <td><?=$device->status == 1 ? 'on' : 'off';?></td>
                        <td><a class="btn btn-sm btn-primary"  href="<?=site_url().'device/edit/'.$device->id?>">Edit</a></td>
                      </tr>
                    <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
    </div>
</div>