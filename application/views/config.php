<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title">Edit Config</h4>
                        <p class="card-category">Complete your profile</p>
                    </div>
                    <div class="card-body">
                        <form action="<?=site_url();?>config/update/<?php echo $config->id;?>" method="POST">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Batas Kelembaban Tanah</label>
                                        <input type="text" name="treshold_moisture"
                                            value="<?php echo !empty($config) ? $config->treshold_moisture : "";?>"
                                            class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Durasi Penyiraman (menit)</label>
                                        <input type="text" name="waktu_siram"
                                            value="<?php echo !empty($config) ? $config->waktu_siram : "";?>"
                                            class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-check form-check-radio">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="mode" type="radio"
                                        id="exampleRadios1" value="0" <?php echo !empty($config->mode)  == 0  ? "checked" : "";?>>
                                    Timer is off
                                    <span class="circle">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>
                            <div class="form-check form-check-radio">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="mode" type="radio" 
                                        id="exampleRadios2" value="1"  <?php echo !empty($config->mode)  == 1  ? "checked" : "";?>>
                                    Timer is on
                                    <span class="circle">
                                        <span class="check"></span>
                                    </span>
                                </label>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Hour 1</label>
                                        <input type="text" name="hour_one"
                                            value="<?php echo !empty($config) ? $config->hour_one : "";?>"
                                            class="form-control datetimepicker">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Hour 2</label>
                                        <input type="text" name="hour_two"
                                            value="<?php echo !empty($config) ? $config->hour_two : "";?>"
                                            class="form-control datetimepicker">
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary pull-right">Update Config</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card card-profile">
                    <div class="card-header card-header-danger card-header-icon">
                        <div class="card-icon">
                            <i class="material-icons">settings</i>
                        </div>
                    </div>
                    <div class="card-body">
                        <h6 class="card-category text-gray">Device</h6>
                        <h4 class="card-title">Device001</h4>
                        <p class="card-description">
                            Don't be scared of the truth because we need to restart the human foundation in truth And I
                            love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
                        </p>
                        <a href="#" class="btn btn-primary btn-round">Edit</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>