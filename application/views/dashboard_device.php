
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <!-- <div class="col-12">
            <button type="button" class="btn btn-lg btn-info">Device <?=$id;?></button>

            </div> -->
            <!-- <div class="col-lg-4 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-warning card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">fireplace</i>
                  </div>
                  <p class="card-category">Temprature</p>
                  <h3 class="card-title">Temprature
                    <small>28 &#8451;</small>
                  </h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">date_range</i> Last 24 Hours
                  </div>
                </div>
              </div>
            </div> -->
            <div class="col-lg-4 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-info card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">assessment</i>
                  </div>
                  <p class="card-category">Device</p>
                  <h3 class="card-title">Device <?=$id;?></h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">date_range</i> Last 24 Hours
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">eco</i>
                  </div>
                  <p class="card-category">Status</p>
                  <h3 class="card-title"> Status <span class="alert-link"><?=$status->status == 0 ? "OFF": "ON";?></span> <a href="<?=site_url();?>dashboard/status/<?=$id;?>/1" class="btn btn-sm btn-info">On</a> |<a href="<?=site_url();?>dashboard/status/<?=$id;?>/0" class="btn btn-sm btn-danger">Off</a></h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">date_range</i> Last 24 Hours
                  </div>
                </div>
              </div>
            </div>
            <!-- <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-danger card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">invert_colors</i>
                  </div>
                  <p class="card-category">Flow</p>
                  <h3 class="card-title">Flow <small>80</small></h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">date_range</i> Last 24 Hours
                  </div>
                </div>
              </div>
            </div> -->
          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="card card-chart">
                <div class="card-header card-header">
                  <canvas id="suhu" width="100" height="50"></canvas>
                </div>
                <div class="card-body">
                  <h4 class="card-title">Temprature</h4>
                  <p class="card-category">
                    <span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in today suhu.</p>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">access_time</i> updated 4 minutes ago
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card card-chart">
                <div class="card-header card-header">
                  <canvas id="moisture" width="100" height="50"></canvas>
                </div>
                <div class="card-body">
                  <h4 class="card-title">Moisture</h4>
                  <p class="card-category"><span class="text-success"><i class="fa fa-long-arrow-up"></i> 20% </span> increase in today moisture.</p>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">access_time</i> updated 4 minutes ago
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card card-chart">
                <div class="card-header card-header">
                  <canvas id="humidity" width="100" height="50"></canvas>
                </div>
                <div class="card-body">
                  <h4 class="card-title">Humidity</h4>
                  <p class="card-category"><span class="text-danger"><i class="fa fa-long-arrow-down"></i> 55% </span> decrease in today flow.</p>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">access_time</i> campaign sent 2 days ago
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12 col-md-12">
              <div class="card">
                <div class="card-header card-header-warning">
                  <h4 class="card-title">Device</h4>
                  <p class="card-category">New employees on 15th September, 2016</p>
                </div>
                <div class="card-body table-responsive">
                  <table class="table table-hover">
                    <thead class="text-warning">
                      <th>Device</th>
                      <th>Moisture</th>
                      <th>Humidity</th>
                      <th>Temprature</th>
                      <th>Date</th>
                    </thead>
                    <tbody>
                    <?php foreach($device as $dev) { ?>
                      <tr>
                        <td><?=$dev->id_controller;?></td>
                        <td><?=$dev->moisture_contain;?></td>
                        <td><?=$dev->humidity;?></td>
                        <td><?=$dev->temperature;?></td>
                        <td><?=$dev->date;?></td>
                      </tr>
                    <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

<script src="<?=base_url();?>assets/js/plugins/Chart.min.js"></script>
<script>
var ctx = document.getElementById('suhu');
var suhu = new Chart(ctx, {
    type: 'line',
    data: {
        labels: [<?php foreach($device as $one) { ?>
              <?php echo "'". $one->second ."',"; } ?>],
        datasets: [{
            data: [<?php foreach($device as $one) { ?>
              <?php echo "'". $one->temperature ."',"; } ?>],
            fill: false,
            backgroundColor: "red",
            borderColor: "red",
            borderWidth: 3
        }]
    },
    options: {
        legend: {
            display: false
        },

        elements: {
            line: {
                tension: 0
            }
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

var ctx = document.getElementById('moisture');
var moisture = new Chart(ctx, {
    type: 'line',
    data: {
        labels: [<?php foreach($device as $one) { ?>
              <?php echo "'". $one->second ."',"; } ?>],
        datasets: [{
            data: [<?php foreach($device as $one) { ?>
              <?php echo "'". $one->moisture_contain ."',"; } ?>],
            fill: false,
            backgroundColor: "green",
            borderColor: "green",
            borderWidth: 3
        }]
    },
    options: {
        legend: {
            display: false
        },

        elements: {
            line: {
                tension: 0
            }
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});


var ctx = document.getElementById('humidity');
var humidity = new Chart(ctx, {
    type: 'line',
    data: {
        labels: [<?php foreach($device as $one) { ?>
              <?php echo "'". $one->second ."',"; } ?>],
        datasets: [{
            data: [<?php foreach($device as $one) { ?>
              <?php echo "'". $one->humidity ."',"; } ?>],
            fill: false,
            backgroundColor: "blue",
            borderColor: "blue",
            borderWidth: 3
        }]
    },
    options: {
        legend: {
            display: false
        },

        elements: {
            line: {
                tension: 0
            }
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
setTimeout(function(){
   window.location.reload(1);
}, 10000);
</script>