<?php
function sqlDate($date) {
    return date('Y-m-d', strtotime($date));
}

function indoDate($date) {
    return date('d-m-Y', strtotime($date));
}

function getTimeOnly($datetime) {
    return date('H:i:s', strtotime($datetime));
}

function nowTime() {
    return  date('Y-m-d H:i:s');
}

function rupiah($angka){
	$hasil_rupiah = "Rp " . number_format($angka,0,',','.');
	return $hasil_rupiah;
}