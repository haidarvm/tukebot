<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class HomeModel extends CI_Model {
    private $tb_logging = 'logging';
    private $tb_link = 'link';



    public function getDevice($id_controller) {
        $this->db->order_by("date", "ASC");
        $this->db->limit(10);
        $this->db->select("DATE_FORMAT(date, \"%H:%i\") as second, moisture_contain, date, flow, temperature, humidity, id_controller");
        $this->db->group_by(" FLOOR(`date` / 200)");
        $query = $this->db->get_where($this->tb_logging, ['id_controller' => $id_controller]);
        // echo $this->db->last_query();exit;
        return $query->result();
    }

    public function getAllLog() {
        $this->db->order_by("date", "DESC");
        $this->db->limit(15);
        $this->db->select("DATE_FORMAT(date, \"%H:%i\") as second, moisture_contain, date, flow, temperature, humidity, id_controller");
        $this->db->group_by(" FLOOR(`date` / 200)");
        $query = $this->db->get_where($this->tb_logging);
        // echo $this->db->last_query();exit;
        return $query->result();
    }


    public function getStatus($id) {
       $query =  $this->db->get_where($this->tb_link, ['id' => $id]);
    //    echo $this->db->last_query();exit;
       return $query->row();
    }

    public function setStatus($id, $status) {
        return $this->db->update($this->tb_link, ['status' => $status], "id =$id");
    }

}