<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class ConfigModel extends CI_Model {
    private $tb_config = 'config';
    private $tb_batas_sensor = 'batas_sensor';
    private $tb_link = 'link';

    public function getConfig($id) {
        $query = $this->db->get_where($this->tb_batas_sensor, ['id' => $id]);
        return $query;
    }

    public function updateConfig($data, $id) {
      return  $this->db->update($this->tb_batas_sensor, $data, ['id' => $id]);
    }


    public function getAllDevice() {
        $query = $this->db->get_where($this->tb_link);
        return $query->result();
    }

    public function getDevice($id) {
        $query = $this->db->get_where($this->tb_link, ['id' => $id]);
        return $query->row();
    }

    public function updateDevice($data, $id) {
        $query = $this->db->update($this->tb_link, $data,  ['id' => $id]);
        return $query;
    }

    public function insertDevice($data) {
        $query = $this->db->insert($this->tb_link, $data);
        return $query;
    }




}