<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Admin_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
         
        if (false == $this->session->userdata('login')) {
            redirect('auth');
        } else {
            $this->title = 'Admin ANNAS';
            $this->user_id = $this->session->userdata('user_id');
            // echo $this->user_id;exit;
            $this->user_login = $this->session->userdata('user_login');
            $this->display_name = $this->session->userdata('display_name');
        }
    }

    public function admin_only() {
        // $admin = [1, 0];
        // if (in_array($this->acl_id, $admin)) {
        //     return true;
        // }
        redirect('/home');
    }
}