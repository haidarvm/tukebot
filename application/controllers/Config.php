<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Config extends CI_Controller {
    protected $id;

    public function __construct() {
        parent::__construct();
        $this->id = $this->uri->segment(3);
        $this->load->model('ConfigModel', 'mconfig', true);
        $this->load->library('pagination');
    }

    public function device($device_id) {

        // echo "haidar chart js
        $data['title'] = "Config";
        $data['config'] = $this->mconfig->getConfig($device_id)->row();
        $this->load->template('config', $data);
    }

    // GET /member
    public function index() {
        
        $this->device(1);
    }

    public function edit($device_id) {
        // echo "haidar chart js
        $data['title'] = "Keuangan";
        $data['action'] = "update/".$device_id;
        // $data['finance'] = $this->mfinance->getFinance($finance_id)->row();
        // $data['finance_types'] = $this->mfinance->getFinanceType();
        // $data['groups'] = $this->mfinance->getAllGroups();
        // $this->load->view("datatables");
        $this->load->template('config', $data);
    }
    
    public function insert() {
        $data = $this->input->post();
        // print_r($data);exit;
        // $insert_id = $this->mfinance->insert($data);
        // redirect('finance/finance_id/'. $data['type_id'].'/'. $data['group_id']);
    }

    public function update($id) {
        $data = $this->input->post();
        // print_r($data);exit;
        $this->mconfig->updateConfig($data, $id);
        redirect('dashboard');
    }


  

}