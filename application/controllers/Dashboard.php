<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Dashboard extends CI_Controller {
    protected $id;

    public function __construct() {
        parent::__construct();
        $this->load->library('user_agent');
        $this->id = $this->uri->segment(3);
        $this->load->model('HomeModel', 'mhome', true);
        $this->load->library('pagination');
    }

    // GET /member
    public function index() {
        $this->home();
    }

    public function home() {
        // echo "haidar chart js
        $data['title'] = "Dashboard";
        $data['device01'] = $this->mhome->getDevice(1);
        $data['device02'] = $this->mhome->getDevice(2);
        $data['devices'] = $this->mhome->getAllLog();
        $this->load->template('dashboard', $data);
    }

    public function status($id, $status) {
        $data['status'] = $this->mhome->getStatus($id);
        $link = $data['status']->link;
        $ch = curl_init(); 
        $this->mhome->setStatus($id, $status);
        // set url 
        $status_link = $status == 1 ? "ON" : "OFF";
        $curl_link = $link.'/?'. $status_link;
        curl_setopt($ch, CURLOPT_URL, $curl_link);
        // echo $curl_link;exit;
        // return the transfer as a string 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

        // $output contains the output string 
        $output = curl_exec($ch); 

        // tutup curl 
        curl_close($ch);      

        // menampilkan hasil curl
        // echo $output;
        redirect($this->agent->referrer());
        if ($this->agent->is_referral())
        {
            echo $this->agent->referrer();
        }
    }



    public function device($id) {
        // echo "haidar chart js
        $data['title'] = "Dashboard001";
        $data['id'] = $id;
        $data['device'] = $this->mhome->getDevice($id);
        $data['status'] = $this->mhome->getStatus($id);
        $this->load->template('dashboard_device', $data);
    }

  

}