<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Device extends CI_Controller {
    protected $id;

    public function __construct() {
        parent::__construct();
        $this->id = $this->uri->segment(3);
        $this->load->model('ConfigModel', 'mconfig', true);
        $this->load->library('pagination');
    }


    // GET /member
    public function index() {
        
        $this->list();
    }

    public function list() {

        // echo "haidar chart js
        $data['title'] = "Config";
        $data['devices'] = $this->mconfig->getAllDevice();
        $this->load->template('devices', $data);
    }

           // GET /member
    public function add() {
        $data['title'] = "Device";
        $data['action'] = "insert";

        // $this->load->view("datatables");
        $this->load->template('device_form', $data);
    }

    public function edit($id) {
        // echo "haidar chart js
        $data['title'] = "Controller";
        $data['action'] = "update/".$id;
        $data['device'] = $this->mconfig->getDevice($id);
        // $this->load->view("datatables");
        $this->load->template('device_form', $data);
    }
    
    public function insert() {
        $data = $this->input->post();
        // print_r($data);exit;
        $insert_id = $this->mconfig->insertDevice($data);
        redirect('device/');
    }

    public function update($device_id) {
        $data = $this->input->post();
        // print_r($data);exit;
        $this->mconfig->updateDevice($data, $device_id);
        redirect('device/');
    }



}